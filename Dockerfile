FROM node:carbon
RUN mkdir -p /app
WORKDIR /app
COPY ./webserver /app
RUN npm install
CMD ["npm", "start"]
