'use strict'
var R = require('ramda');
var log = require('../bin/logger');
var dao = require('../service/dao');

class Auth{
  constructor(){
    log.info('init Login');
  }

  fbLogin(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      if (!R.hasPath(['body', 'fb_token'], req)) {
        throw new Error('cannot find fb_token in request.');
      }
      if (R.isNil(req.body.fb_token)){
        throw new Error('fb_token is undefined.');
      }
      return req.body.fb_token;
    })
    .then( fb_token => {
      return dao.selectUserByFBToken(fb_token);      
    })
  }
  
}

module.exports = new Auth();
