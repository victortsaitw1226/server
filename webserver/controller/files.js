'use strict'
var request = require('request');
var rp = require('request-promise');
var log = require('../bin/logger');
var config = require('../bin/config');
var R = require('ramda');
var hash = require('../bin/hash');
var path = require('path');
var dao= require('../service/dao');

class Files {
  constructor(){
    log.info('init Files');
  }

  fetchFile(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      // if (!req.session.user) throw Error('User does not login');
      if (!R.hasPath(['params', 'filename'], req)){
        throw new Error('cannot find params.filename in request');
      }
      if (!R.hasPath(['params', 'folder'], req)){
        throw new Error('cannot find params.folder in request');
      }
      if (R.isNil(req.params.filename)){
        throw new Error('req.params.filename is undefined');
      }
      if (R.isNil(req.params.folder)){
        throw new Error('req.params.folder is undefined');
      }
      return {folder: req.params.folder, filename: req.params.filename};
    })
    .then( ({folder, filename}) => {
      if(R.or(R.not(config.id_check),
        R.equals('examples', folder))
      ) {
        return {folder: folder, filename: filename};
      }
      var predict_material = '';
      if(filename.match(/^img-/i)){
        predict_material = filename.split('img-').pop();
      }
      if(filename.match(/^small-img-/i)){
        predict_material = filename.split('small-img-').pop();
      }
      log.debug(predict_material);
      return dao.selectPredictResultBySessionAndPredictMaterial(
          req.session.id, predict_material
      ).then( _ => {
        return {folder: folder, filename: filename};
      });
    })
    .then( ({folder, filename}) => {
      var filename= req.params.filename;
      return config.replace({
          'filename': folder + '/' + filename
        }, config.depends.file_img_url);
    })
    .then( file_uri => {
      return rp({ url: file_uri, encoding: null});
    });
  } 
}

module.exports = new Files();
