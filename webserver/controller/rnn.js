'use strict'
var model = require('../service/model');
var dao  = require('../service/dao');
var log = require('../bin/logger');
var config = require('../bin/config');
var hash = require('../bin/hash');
var R = require('ramda');
var path = require('path');
var RnnResult = require('../bin/result').RnnResult;

class RNN {
  constructor(){
    log.info('init rnn');
  }
  predict(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      // Validate input data
      // if (!req.session.user) throw Error('User does not login');
      return v;
    })
    .then( info => {
      // Compose data to predict
      return {
        predict_data: {
          predict_model: 'rnn',
          predict_material: req.body.word
        }
      }
    })
    .then( info => {
      // Send to the model server to predict
      return model.predict(config.depends.rnn_predict_url,
                           info.predict_data,
                           info);
    })
    .then( ({response, url, info}) => {
      if ( RnnResult.CODE_WORDS_NOT_FOUND == response.result ) {
        throw new RnnResult({prediction: "", ref_url: "", error: response.data});
      }

      if ( RnnResult.CODE_PRIDICT_NOT_END == response.result ) {
        throw new RnnResult({prediction: "", ref_url: "", error: null});
      }

      if (0 != response.result) {
        throw new Error('response Fail:' + response.message);
      }

      return {
        predict: response.data,
        url: url,
        data: info
      }
    })
    .then( ({predict, url, data}) => {
      var originalUrl = req.originalUrl.replace(URL_ROOT_PATH, '');
      var urls = R.split('/', originalUrl);
      return dao.insertLog({
        req: req,
        predict_material: req.body.word,
        succint_result: predict,
        data: {
          'result': predict, 
          'type': urls[1],
          'algorithm': urls[2],
          'dataset': urls[3],
          'word': req.body.word,
        }
      });
    })
    .then( ({predict_id, data}) => {
      // return result to the user
      return {
        prediction: data.result,
        ref_url: path.join(URL_ROOT_PATH,
                           data.type,
                           data.algorithm,
                           data.dataset,
                           'html',
                           hash.encrypt(predict_id))
      }
    })
    .catch( error => {
      if (R.is(RnnResult, error)){
        return error.get_data();
      }
      throw error; 
    });
  }

  fetchHistory(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      if (!R.hasPath(['params', 'hash'], req)){
        throw new Error('cannot find params.hash in request');
      }
      if (R.isNil(req.params.hash)){
        throw new Error('req.params.hash is undefined');
      }
      return v;
    })
    .then( v => {
      return hash.decrypt(req.params.hash);
    })
    .then( hash_code => {
      if(config.id_check){
        return dao.selectPredictResultBySessionAndHashCode(
          req.session.id, hash_code
        );
      }
      return dao.selectPredictResultByHashCode(hash_code);
    })
    .then( data => {
      return {history: data};
    })
  }
}

module.exports = new RNN();
