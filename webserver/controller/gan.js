'use strict'
var model = require('../service/model');
var dao  = require('../service/dao');
var log = require('../bin/logger');
var config = require('../bin/config');
var hash = require('../bin/hash');
var R = require('ramda');
var path = require('path');

class GAN {
  constructor(){
    log.info('init rnn');
  }
  predict(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      // Validate input data
      // if (!req.session.user) throw Error('User does not login');
      return v;
    })
    .then( info => {
      // Compose data to predict
      var predict_material = {
        parameters: [
          req.body.parameter1, 
          req.body.parameter2, 
          req.body.parameter3, 
          req.body.parameter4, 
          req.body.parameter5, 
        ],
        image_name: req.id + '.png'
      }
      return {
        predict_data: {
          predict_model: 'gan',
          predict_material: JSON.stringify(predict_material)
        }
      }
    })
    .then( info => {
      // Send to the model server to predict
      return model.predict(config.depends.gan_predict_url,
                           info.predict_data,
                           info);
    })
    .then( ({response, url, info}) => {
      if (0 != response.result) {
        throw new Error('response Fail:' + response.message);
      }
      log.debug(response);
      log.debug(JSON.stringify(response));
      return {
        predict: path.join(URL_ROOT_PATH, 'files', response.data.sub_folder, response.data.filename),
        url: url,
        data: info
      }
    })
    .then( ({predict, url, data}) => {
      // save to database
      var str_predict_material = data.predict_data.predict_material;
      var originalUrl = req.originalUrl.replace(URL_ROOT_PATH, '');
      var urls = R.split('/', originalUrl);
      return dao.insertLog({
        req: req,
        predict_material: req.id + '.png',
        succint_result: predict,
        data: {
          'result': predict, 
          'type': urls[1],
          'algorithm': urls[2],
          'dataset': urls[3],
          'parameter': str_predict_material,
        }
      });
    })
    .then( ({predict_id, data}) => {
      // return result to the user
      return {
        prediction: data.result,
        ref_url: path.join(URL_ROOT_PATH, data.type, data.algorithm, data.dataset, 'html', hash.encrypt(predict_id))
      }
    });
  }
  fetchHistory(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      if (!R.hasPath(['params', 'hash'], req)){
        throw new Error('cannot find params.hash in request');
      }
      if (R.isNil(req.params.hash)){
        throw new Error('req.params.hash is undefined');
      }
      return v;
    })
    .then( v => {
      return hash.decrypt(req.params.hash);
    })
    .then( hash_code => {
      if(config.id_check){
        return dao.selectPredictResultBySessionAndHashCode(
          req.session.id, hash_code
        );
      }
      return dao.selectPredictResultByHashCode(hash_code);
    })
    .then( data => {
      return {history: data};
    })
  }
}

module.exports = new GAN();
