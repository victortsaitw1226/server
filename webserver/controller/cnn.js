'use strict'
var model = require('../service/model');
var upload = require('../service/upload');
var dao= require('../service/dao');
var createError = require('http-errors');
var R = require('ramda');
var log = require('../bin/logger');
var config = require('../bin/config');
var hash = require('../bin/hash');
var path = require('path');

class CNN {
  constructor(){
    log.debug('initial CNN');
  }
  predict(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      // validate input data
      if (!req.file) throw Error('Image is not posted');
      log.info(JSON.stringify(req.session));
      // if (!req.session.user) throw Error('User does not login');
      return v;
    })
    .then( v => {
      // send the image to the file server
      return upload.post_image(config.depends.file_upload_url, req.file);
    })
    .then( info => {
      log.debug('==================');
      log.debug(JSON.stringify(info));
      // Compose data to predict'
      return R.merge(info, {
        predict_data: {
          predict_model: 'cnn',
          predict_material: config.replace({
            'filename': path.join(info.sub_folder, info.resize_info.filename)
          }, config.depends.file_img_url),
        }
      });
    })
    .then( info => {
      // Send to the model server to predict
      return model.predict(config.depends.cnn_predict_url,
                           info.predict_data,
                           info);
    })
    .then( ({response, url, info}) => {
      // Transform the predicted data.
      const predict = R.values(response.data);
      const originalUrl = req.originalUrl.replace(URL_ROOT_PATH, '');
      const urls = R.split('/', originalUrl);
      // save to mysql
      return dao.insertLog({
        req: req,
        predict_material: info.originalname,
        succint_result: JSON.stringify(predict),
        data: {
          'result': predict, 
          'type': urls[1],
          'algorithm': urls[2],
          'dataset': urls[3],
          'img': path.join(URL_ROOT_PATH, 'files', info.sub_folder, info.filename),
          'small-img': path.join(URL_ROOT_PATH, 'files', info.sub_folder, info.resize_info.filename)
        }
      });
    })
    .then( ({predict_id, data}) => {
      log.debug(data);
      return {
        prediction: data.result,
        ref_url: path.join(URL_ROOT_PATH,
                           data.type,
                           data.algorithm,
                           data.dataset,
                           'html',
                           hash.encrypt(predict_id)),
        img: data.img
      }
    });
  }

  fetchHistory(req, res, next){
    return Promise.resolve('start')
    .then( v => {
      if (!R.hasPath(['params', 'hash'], req)){
        throw new Error('cannot find params.hash in request');
      }
      if (R.isNil(req.params.hash)){
        throw new Error('req.params.hash is undefined');
      }
      return v;
    })
    .then( v => {
      return hash.decrypt(req.params.hash);
    })
    .then( hash_code => {
      if(config.id_check){
        return dao.selectPredictResultBySessionAndHashCode(
          req.session.id, hash_code
        );
      }
      return dao.selectPredictResultByHashCode(hash_code);
    })
    .then( data => {
      return {history: data};
    })
  }
}

module.exports = new CNN();
