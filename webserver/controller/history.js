'use strict'
var R = require('ramda');
var dao = require('../service/dao');
var log = require('../bin/logger');
var hash = require('../bin/hash');

class History{
  constructor(){
    log.info('init History');
  }
  fetchHistory(req, res, next){
    return Promise.resolve('insert')
    .then( v => {
      // if (!req.session.user) throw Error('User does not login');
      if (!R.hasPath(['query', 'page'], req)) {
        throw new Error('cannot find query.page in request.');
      }
      if (!R.hasPath(['query', 'count'], req)){
        throw new Error('cannot find query.count in request.');
      }
      if (R.isNil(req.query.page)){
        throw new Error('query.page is undefined.');
      }
      if (R.isNil(req.query.count)){
        throw new Error('query.count is undefined.');
      }

      log.info(JSON.stringify(req.session.user));
      return v;
    })
    .then( v => {
      return {
        page: req.query.page,
        count: req.query.count,
        account_id: req.session.user.account_id
      };
    })
    .then( ({page, count, account_id}) => {
      return dao.selectPagingLogsByAccountID(page, count, account_id)
      .then( results => {
        var history = R.map(r => { return {
          type: r.type,
          result: r.result,
          ref_url: '/' + r.type + '/' + hash.encrypt(r.id)
        }}, results);
        return {account_id, history};
      });
    })
    .then( ({account_id, history}) => {
      return dao.countLogsByAccountId(account_id)
      .then( total => {
        return {total, history};
      })
    })
    .then( ({total, history}) => {
      return {
        history: history,
        total_logs: total
      };
    })
  }
}

module.exports = new History();
