'use strict'
var path = require('path');
var yaml = require('js-yaml');
var fs = require('fs');
var R = require('ramda');

class Config {
  constructor(config_path){
    var doc = yaml.safeLoad(fs.readFileSync(config_path, 'utf8'));
    Object.assign(this, doc);
    this.makeItGlobal();
  }
  replace(obj, target){
    var t = target;
    for(var key of R.keys(obj)){
      var value = obj[key];

      if (R.type(t) !== 'String') continue;

      var re = RegExp('{' + key + '}');
      t = R.replace(re, value, t);
    }
    return t;
  }
  makeItGlobal(){
    var g = this.global;
    for (var k of Object.keys(g)){
      var v = g[k];
      var new_v = this.replace({
        'rootpath': process.cwd() 
      }, v);
      
      global[k] = new_v;
    }
  }
}


module.exports = (function(){
  var singleton_config;
  if (!singleton_config){
    var config_path = process.env.CONFIG_PATH;
    if (!config_path){
      throw Error('Please input config file');
    }
    // check the log folder
    if (!fs.existsSync(config_path)) {
      throw Error('Connot find config');
    }
    singleton_config = new Config(config_path);
  }
  return singleton_config;
})();
