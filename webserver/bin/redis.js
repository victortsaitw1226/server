'use strict'
var R = require('ramda');
var redis = require("redis");
var EventEmitter = require('events');
var log = require('./logger');

class Redis extends EventEmitter {
  constructor(cfg){
    super(); //must call super for "this" to be defined.
    log.info('connect to ' + cfg.db);
    this.state = 'init';
    this.config = cfg;
    this.redis = redis.createClient(cfg.url);

    this.redis.set('key', 'value', 'EX', 10,
      this.onConnection.bind(this));
  }
  
  onConnection(error, results){
    if (error) {
      log.debug('redis connection fail');
      return this.emit('connectionFail', {db: this.config, error: error});
    }
    log.debug('redis connection success');
    this.state = 'run';
    this.emit('ready', {db: this.config, state:this.state});
  }
  connection(){
    return this.redis;
  }
  close(){
    log.warn('Redis Close');
    this.redis.quit(err => {
      log.warn('Redis Connection Close');
    });
  }
}

module.exports = (function(){
  var singleton;
  var config = require('./config');
  if (!singleton){
    var cfg = config.redis;
    if (!cfg){
      log.error('Please input config file');
    } else {
      singleton = new Redis(cfg);
    }
  }
  return singleton;
})();
