'use strict'
var R = require('ramda');
var mysql = require('mysql');
var EventEmitter = require('events');
var log = require('./logger');
var util = require('util');

class DBPool extends EventEmitter {
  constructor(cfg){
    super(); //must call super for "this" to be defined.
    this.state = 'init';
    this.config = cfg;
    this.pool = mysql.createPool({
      host: cfg.host,
      user: cfg.user,
      password: cfg.password,
      database: cfg.db,
      connectionLimit: 10,
      connectTimeout: 1000,
      acquireTimeout: 1000,
      timeout: 1000
    });
    this.pool.query('SELECT 1 + 1 AS solution',
      this.onConnection.bind(this));
    this.pool.query = util.promisify(this.pool.query);
  }
  
  onConnection(error, results, fields){
    var db = this.config.db;
    if (error) {
      return this.emit('connectionFail', {db: db, error: error});
    }
    this.state = 'run';
    this.emit('connectionSuccess', {db: db, state:this.state});
  }
  checkConnection(){
    return 'run' == this.state;
  }
  select(command, args){
    this.show(command, args);
    return this.pool.query(command, args);  
  }
  insert(command, args){
    this.show(command, args);
    return this.pool.query(command, args);  
  }
  show(command, args){
    log.info(mysql.format(command, args));
  }
  close(){
    this.pool.end( err => {
      if (err){
         return log.error(err);
      }
      log.warn('DB Connection Close');
    });
  }
}

class DBConnection extends EventEmitter {
  constructor(db_config){
    super();
    this.db = {};
    log.info(JSON.stringify(db_config));
    for(var cfg of db_config){
      log.info(JSON.stringify(cfg));
      var db_name = cfg.db;
      this.db[db_name] = new DBPool(cfg);
      this.db[db_name].on('connectionSuccess', 
        R.bind(this.onConnectionSuccess, this));
      this.db[db_name].on('connectionFail',
        R.bind(this.onConnectionFail, this));
    }
  }

  checkConnection(){
    var li = [];
    for(var db of Object.values(this.db)){
      li.push(db.checkConnection());
    }
    log.info(li);
    if (R.all(R.T)(li)){
      this.emit('ready');
    }
  }

  onConnectionSuccess({db, state}){
    log.info('db connection success:' + db + ':' + state);
    this.checkConnection();
  }

  onConnectionFail({db, error}){
    log.info('db connection fail:' + db + ':' + error);
    this.checkConnection();
  }

  connection(db_name){
    return this.db[db_name];
  }
  close(){
    log.warn('DB Pool Close');
    for(var db of Object.values(this.db)){
      db.close();
    }
  }
}

module.exports = (function(){
  var singleton_db;
  var config = require('./config');
  if (!singleton_db){
    var databases = config.databases;
    if (!databases){
      log.error('Please input config file');
    } else {
      singleton_db= new DBConnection(databases);
    }
  }
  return singleton_db;
})();
