'use strict'
var R = require('ramda');

class RnnResult {

  constructor({prediction, ref_url, error}){
    this.prediction = prediction;
    this.ref_url = ref_url;
    this.error = error;
  }
  get_data(){
    var data = {
      prediction: this.prediction,
      ref_url: this.ref_url
    }
    if(this.error){
      return R.merge(data, {error: this.error})
    }    

  }
}
RnnResult.CODE_WORDS_NOT_FOUND = 1;
RnnResult.CODE_PRIDICT_NOT_END = 2;


exports.RnnResult = RnnResult;
