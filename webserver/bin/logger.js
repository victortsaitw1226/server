'use strict'
var path = require('path');
var R = require('ramda');
var fs = require('fs');
var config = require('./config');
var winston = require('winston');
var DailyRotateFile = require('winston-daily-rotate-file');
var os = require('os');

class Logger {

  constructor(){
    // Define Log Format
    var logFormat = winston.format.printf(({ level, message, label, timestamp }) => {
      return `${timestamp}[${label}:${process.pid}]${level}:${message}`;
    });
    // Define Log Configuration
    var common_setting = {
      datePattern: 'YYYMMDD',
      dirname: path.join(LOG_PATH, os.hostname()),
      zippedArchive: true,
      // maxSize: '1g',
      maxFiles: '14d',
      frequency: '24h'
    }
    // Create Transports
    var debug_transport = this.createDebugTransport(common_setting);
    var info_transport = this.createInfoTransport(common_setting);
    var warn_transport = this.createWarnTransport(common_setting);
    var error_transport = this.createErrorTransport(common_setting);
    // Create Logger Instance
    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.label({label: config.name}),
        winston.format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss.SSS'
        }),
        logFormat,
      ),
      transports: [ debug_transport, info_transport, warn_transport, error_transport ],
      exceptionHandlers: [ error_transport ],
      exitOnError: false
    });
    this.logger.stream = {
      write: function(message, encoding) {
        this.logger.info(message);
      },
    };
  }
  exception(err){
    if (err.stack){
      for(var line of err.stack.split('\n')){
        this.logger.error(line);
      }
    }
  }
  debug(msg){
    this.logger.debug(msg);
  }
  info(msg){
    this.logger.info(msg);
  }
  warn(msg){
    this.logger.warn(msg);
  }
  error(msg){
    this.logger.error(msg);
    this.exception(msg);
  }
  createDebugTransport(common_setting){
    return new (winston.transports.DailyRotateFile)(
      Object.assign(common_setting, {
        filename: 'debug.log.%DATE%',
        level: 'debug'
      })
    );
  }
  createInfoTransport(common_setting){
    return new (winston.transports.DailyRotateFile)(
      Object.assign(common_setting, {
        filename: 'info.log.%DATE%',
        level: 'info'
      })
    );
  }
  createWarnTransport(common_setting){
    return new (winston.transports.DailyRotateFile)(
      Object.assign(common_setting, {
        filename: 'warn.log.%DATE%',
        level: 'warn'
      })
    );
  }
  createErrorTransport(common_setting){
    return new (winston.transports.DailyRotateFile)(
      Object.assign(common_setting, {
        filename: 'error.log.%DATE%',
        level: 'error'
      })
    );
  }
}
module.exports = new Logger();
