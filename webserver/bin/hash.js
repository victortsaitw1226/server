'use strict'
var crypto = require('crypto');
class Hash{
  
  constructor(){
    this.algorithm = "aes-256-ctr";
    this.password = 'passwordd6F3Efeq';
  }

  encrypt(text, algorithm, password){
    if (algorithm === undefined) {
        algorithm = "aes-256-ctr";
    }
    if (password === undefined) {
        password = this.password;
    }
    text = text + ':' + password
    var cipher = crypto.createCipher(algorithm, password)
    var crypted = cipher.update(text,'utf8','hex')
    crypted += cipher.final('hex');
    return crypted;
  }
 
  decrypt(text, algorithm, password){
    if (algorithm === undefined) {
        algorithm = this.algorithm;
    }
    if (password === undefined) {
        password = this.password
    }
    var decipher = crypto.createDecipher(algorithm, password)
    var dec = decipher.update(text,'hex','utf8')
    dec += decipher.final('utf8');
    dec = dec.split(':' + password)[0];
    return dec;
  }

  encryptBuffer(buffer, algorithm, password){
    if (algorithm === undefined) {
        algorithm = this.algorithm;
    }
    if (password === undefined) {
        password = this.password
    }
    var cipher = crypto.createCipher(algorithm,password)
    var crypted = Buffer.concat([cipher.update(buffer),cipher.final()]);
    return crypted;
  }
 
  decryptBuffer(buffer, algorithm, password){
    if (algorithm === undefined) {
        algorithm = this.algorithm;
    }
    if (password === undefined) {
        password = this.password
    }
    var decipher = crypto.createDecipher(algorithm,password)
    var dec = Buffer.concat([decipher.update(buffer) , decipher.final()]);
    return dec;
  }
}

module.exports = new Hash();
