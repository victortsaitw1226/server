'use strict'
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var morgan= require('morgan');
var uuid = require('uuid');
var R = require('ramda');

/*
 * 
 */
var log = require('./bin/logger');
var redis = require('./bin/redis');

/*
 *  Require Router
 */
var indexRouter = require('./routes/index');
var historyRouter = require('./routes/history');
var cnnRouter = require('./routes/cnn');
var rnnRouter = require('./routes/rnn');
var ganRouter = require('./routes/gan');
var audioRouter = require('./routes/audio');
var discoverRouter = require('./routes/discover');
var filesRouter = require('./routes/files');
var statusRouter = require('./routes/status');

var app = express();

var session_config = {
  secret: 'taiwanbookaiwebserver2018',
  resave: false,
  saveUninitialized: true,
  rolling: true,
  cookie: { maxAge: global.SESSION_AGE || 5000}
} 
if (redis){
  log.info('session with redis');
  session_config = Object.assign(session_config,{
    store: new RedisStore({
      client: redis.connection()
    }),
  });
}

// view engine setup
app.set('views', path.join(__dirname, 'template'));
app.set('view engine', 'ejs');

//app.use(morgan('dev'));
app.use(morgan('combined', { stream: log.stream }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(session(session_config));
app.use(function(req, res, next){
  req.session.touch();
  next();
});
app.use(function(req, res, next){
  req.id = R.replace(/-/g, '', uuid.v4());
  next();
});

app.use('/status', statusRouter);
app.use(URL_ROOT_PATH + '/', indexRouter);
app.use(URL_ROOT_PATH + '/static', express.static(path.join(__dirname, 'static')));
app.use(URL_ROOT_PATH + '/cnn', cnnRouter);
app.use(URL_ROOT_PATH + '/rnn', rnnRouter);
app.use(URL_ROOT_PATH + '/gan', ganRouter);
app.use(URL_ROOT_PATH + '/audio', audioRouter);
app.use(URL_ROOT_PATH + '/discover', discoverRouter);
app.use(URL_ROOT_PATH + '/history', historyRouter);
app.use(URL_ROOT_PATH + '/files', filesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {

  log.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
  log.error(err);

  // special error
  if (err.message == 'SESSION_PREDICT_ID_NOT_FOUND'){
    res.status(403);
  } else {
    res.status(err.status || 500);
  }

  res.json({
    message: err.message,
    error: err
  })
  // set locals, only providing error in development
  //res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  //res.status(err.status || 500);
  //res.render('error');
});

module.exports = app;
