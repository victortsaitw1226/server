var request = require('supertest');
var chai = require('chai');
var expect = chai.expect;
var assert = require('assert');
var app = require('../app');
var nock = require('nock');

describe('Testing file server', () =>{
  describe('Testing Post Image', () => {
    describe('check file size', () => {
      it('big size', (done) => {
         request(app)
          .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
          .attach('image', 'tests/fixtures/testBigImg.jpg')
          .then(res => {
            expect(res.status).to.be.equal(500);
            expect(res.body.message).to.be.equal('File too large');
            done();
          })
          .catch(err => {
            done(err);
          });
      });
    });
    describe('check the type of file', () => {
      it('csv', (done) => {
         request(app)
          .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
          .attach('image', 'tests/fixtures/testCSV.csv')
          .then(res => {
            expect(res.status).to.be.equal(500);
            expect(res.body.message).to.be.equal('Only images are allowed');
            done();
          })
          .catch(err => {
            done(err);
          });
      });
      it('pdf', (done) => {
         request(app)
          .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
          .attach('image', 'tests/fixtures/testPDF.pdf')
          .then(res => {
            expect(res.status).to.be.equal(500);
            expect(res.body.message).to.be.equal('Only images are allowed');
            done();
          })
          .catch(err => {
            done(err);
          });
      });
    });
  });
  describe('Testing Reponse 500', () => {
    before(function(){
      nock('http://filemanager:3001').post(/^\/files\/upload/)
      .replyWithError(
        'something went wrong'
      );
    });
    it('test error', (done) => {
       request(app)
        .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
        .attach('image', 'tests/fixtures/cat.jpg')
        .then(res => {
          expect(res.status).to.be.equal(500);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
  describe('Testing Timeout', () =>{
    before(function(){
      nock('http://filemanager:3001').post(/^\/files\/upload/)
      .delay(10000)
      .reply(200);
    });
    it('test error', (done) => {
       request(app)
        .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
        .attach('image', 'tests/fixtures/cat.jpg')
        .then(res => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('Error: ESOCKETTIMEDOUT');
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
})
