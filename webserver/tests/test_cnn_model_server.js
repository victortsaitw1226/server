var request = require('supertest');
var chai = require('chai');
var expect = chai.expect;
var assert = require('assert');
var app = require('../app');
var nock = require('nock');

describe('Testing model server', () =>{
  describe('Testing Reponse 500', () => {
    before(function(){
      nock('http://filemanager:3001').post(/^\/files\/upload/)
      .reply(200, {filename: 'xxx.jpg', resize_info: {filename: 'xxx.jpg'}});
      nock('http://modelserver:5000').post(/^\/predict/)
      .replyWithError(
         'something went wrong'
      );
    });
    it('test error', (done) => {
       request(app)
        .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
        .attach('image', 'tests/fixtures/cat.jpg')
        .then(res => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('something went wrong');
          console.log(res.body.message);
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
  describe('Testing Timeout', () => {
    before(function(){
      nock('http://filemanager:3001').post(/^\/files\/upload/)
      .reply(200, {filename: 'xxx.jpg', resize_info: {filename: 'xxx.jpg'}});
      nock('http://modelserver:5000').post(/^\/predict/)
      .delay(10000)
      .reply(200);
    });
    it('test error', (done) => {
       request(app)
        .post('/textbook/interactive/cnn/vgg19/cifar10/predict')
        .attach('image', 'tests/fixtures/cat.jpg')
        .then(res => {
          expect(res.status).to.be.equal(500);
          expect(res.body.message).to.be.equal('timeout of 5000ms exceeded');
          done();
        })
        .catch(err => {
          done(err);
        });
    });
  });
})
