'use strict'
// CNN
var express = require('express');
var router = express.Router();

/* 
 * ====================
 * HTML routes
 * ====================
 */

router.get('/index', function(req, res, next) {
  return res.render('discover/html', {URL_ROOT_PATH: URL_ROOT_PATH});
});

/* 
 * ====================
 * JSON routes
 * ====================
 */

module.exports = router;

