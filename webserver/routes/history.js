// CNN
var express = require('express');
var router = express.Router();
var history = require('../controller/history');

router.get('/', function(req, res, next) {
  return history.fetchHistory(req, res, next)
  .then(({history, total_logs}) => {
    res.json({
      history: history,
      total_logs: total_logs
    });
  })
  .catch( err => {
    next(err);
  });
});

module.exports = router;
