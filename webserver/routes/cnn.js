'use strict'
// CNN
var express = require('express');
var router = express.Router();
var upload = require('../service/upload');
var cnn = require('../controller/cnn');
var data = require('../static/json/cnn.json');

Object.keys(data).forEach(function(key) {
  var value = data[key];

  value.items.forEach(function(item_value){
    item_value.hash_url = URL_ROOT_PATH + item_value.hash_url;
    item_value.image_url = URL_ROOT_PATH + item_value.image_url;
  });
});

/*
 * api: http://host/cnn/vgg19/cifar10/predict
 * action: post
 * value: image
 * response: predict data
 */
router.post('/vgg19/cifar10/predict', upload.disk.single('image'), function(req, res, next) {
  return cnn.predict(req, res, next)
  .then(({prediction, ref_url, img}) => {
    res.json({
      result: prediction,
      url: ref_url,
      img: img
    });
  })
  .catch(err => {
    next(err);
  });
});


/* 
 * ====================
 * HTML routes
 * ====================
 */
router.get('/vgg19/cifar10/home/index', function(req, res, next) {
  return res.render('cnn/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'home',
                                 pageparam: 'index'});
});

router.get('/vgg19/cifar10/examples/:filename', function(req, res, next) {
  return res.render('cnn/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'examples',
                                 pageparam: req.params.filename});
});

router.get('/vgg19/cifar10/html/:hash', function(req, res, next) {
  return res.render('cnn/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'html',
                                 pageparam: req.params.hash});
});

/* 
 * ====================
 * JSON routes
 * ====================
 */

router.get('/vgg19/cifar10/json/index', function(req, res, next) {
  res.set('content-type', 'application/json');
  return res.end(JSON.stringify(data));
});

/*
 * http://host/cnn/vgg19/cifar10/json/xxxxxxxx
 * action: get
 * response: predict history data
 */
router.get('/vgg19/cifar10/json/:hash', function(req, res, next) {
  return cnn.fetchHistory(req, res, next)
  .then(({history}) => {
    res.json(history);
  })
  .catch(err => { 
    next(err); 
  });
});

module.exports = router;

