'use strict'
var express = require('express');
var router = express.Router();
var auth = require('../controller/auth');

router.get('/', function(req, res, next) {
  return Promise.resolve('start')
  .then( v => {
    res.render('index', {URL_ROOT_PATH: URL_ROOT_PATH});
  })
  .catch( err =>{
    next(err);
  });
});

router.post('/login', function(req, res, next){
  return auth.fbLogin(req, res, next)
  .then( user => {
    req.session.user = user;
    res.json(user);
  })
  .catch( err => {
    next(err);
  });
});

router.get('/logout', function(req, res, next){
  return Promise.resolve('start')
  .then( v => {
    req.session.destroy();
    return res.json({});
  })
  .catch( err => {
    next(err);
  });
});

module.exports = router;
