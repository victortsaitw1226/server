'use strict'
var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  return Promise.resolve('start')
  .then( v => {
    res.json({
      name: 'backend'
    });
  })
  .catch( err =>{
    next(err);
  });
});

module.exports = router;
