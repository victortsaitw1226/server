'use strict'
var express = require('express');
var router = express.Router();
var gan = require('../controller/gan');

/*
 * api: http://host/gan/stylegan/ffgh/predict
 * action: post
 * value: parameter
 * response: predict data
 */
router.post('/stylegan/ffhq/predict',  function(req, res, next) {
  return gan.predict(req, res, next)
  .then( ({prediction, ref_url}) => {
    res.json({
      result: prediction,
      url: ref_url
    });
  })
  .catch(err => {
    next(err);
  });
});

/* 
 * ====================
 * HTML routes
 * ====================
 */
router.get('/stylegan/ffhq/home/index', function(req, res, next) {
  return res.render('gan/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'home',
                                 pageparam: 'index'});
});

router.get('/stylegan/ffhq/html/:hash', function(req, res, next) {
  return res.render('gan/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'html',
                                 pageparam: req.params.hash});
});

/* 
 * ====================
 * JSON routes
 * ====================
 */

/*
 * http://host/gan/xxxxxxxx
 * action: get
 * response: predict history data
 */
router.get('/stylegan/ffhq/json/:hash', function(req, res, next) {
  return gan.fetchHistory(req, res, next)
  .then(({history}) => {
    res.json(history);
  })
  .catch(err => { 
    next(err); 
  });
});

module.exports = router;
