'use strict'
var express = require('express');
var router = express.Router();
var files = require('../controller/files');

/*
 * api: http://host/xxxfolder/xxfile
 * action: get
 * value: filename
 * response: file
 */
router.get('/:folder/:filename', function(req, res, next) {
  return files.fetchFile(req, res, next)
  .then( file => {
    return res.end(file, 'binary');
  })
  .catch( err => {
    next(err);
  });
});

module.exports = router;
