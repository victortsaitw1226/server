'use strict'
var express = require('express');
var router = express.Router();
var rnn = require('../controller/rnn');

/*
 * api: http://host/rnn/lstm/5455945/predict
 * action: post
 * value: word
 * response: predict data
 */
router.post('/lstm/5455945/predict',  function(req, res, next) {
  return rnn.predict(req, res, next)
  .then( ({prediction, ref_url, error}) => {
    res.json({
      result: prediction,
      url: ref_url,
      error: error
    });
  })
  .catch(err => {
    next(err);
  });
});

/* 
 * ====================
 * HTML routes
 * ====================
 */

router.get('/lstm/5455945/home/index', function(req, res, next) {
  return res.render('rnn/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'home',
                                 pageparam: 'index'});
});

router.get('/lstm/5455945/html/:hash', function(req, res, next) {
  return res.render('rnn/html', {URL_ROOT_PATH: URL_ROOT_PATH,
                                 pagetype: 'html',
                                 pageparam: req.params.hash});
});


/* 
 * ====================
 * JSON routes
 * ====================
 */


/*
 * http://host/rnn/xxxxxxxx
 * action: get
 * response: predict history data
 */
router.get('/lstm/5455945/json/:hash', function(req, res, next) {
  return rnn.fetchHistory(req, res, next)
  .then(({history}) => {
    res.json(history);
  })
  .catch(err => { 
    next(err); 
  });
});

module.exports = router;
