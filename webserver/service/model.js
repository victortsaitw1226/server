const axios = require('axios');
const querystring = require('querystring');
const log = require('../bin/logger');
const rp = require('request-promise');
const R = require('ramda');

class Model {
  constructor(){
    log.info('init Model');
  }

  predict(url, predict_data, info){
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
      },
      timeout: 30000
    }
    return axios.post(url, querystring.stringify(predict_data), config)
    .then( response => {
      return {
        response: response.data.result,
        url: url,
        info: info
      };
    })
    .catch(function (error) {
      if (R.isNil(error.response)){
        throw new Error(error.message);
      }
      var response = error.response;
      log.error(JSON.stringify(response.data));
      log.error(response.status);
      log.error(response.statusText);
      log.error(JSON.stringify(response.headers));
      log.error(JSON.stringify(response.config));
      throw new Error('predict fail');
    });
  }
}

module.exports = new Model();
