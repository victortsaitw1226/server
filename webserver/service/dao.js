'use strict'
var db = require('../bin/dbconnection');
var EventEmitter = require('events');
var R = require('ramda');
var log = require('../bin/logger');

class Dao {
  constructor(){
    log.info('init Dao');
  }

  selectSubPredictionLog({account_id, predict_material, result}){
    return db.connection('aiix')
    .select('SELECT * FROM sub_prediction_logs WHERE account_id = ? AND predict_material LIKE ?',
      [account_id, predict_material, result])
    .then(result => {
      return result[0].id;
    })
    .catch(err => {
      throw err;
    });
  }
  /*
   * Create Sub-Prediction Logs
   */
  insertToSubPredictionLog({
    account_id, 
    predict_material,
    succint_result,
    type, 
    algorithm, 
    dataset, 
    result}){
    return db.connection('aiix')
    .insert('INSERT INTO sub_prediction_logs SET ?', 
      {
        account_id: account_id,
        predict_material: predict_material,
        type: type,
        algorithm: algorithm,
        dataset: dataset,
        succint_result: succint_result,
        result: result,
      })
    .then( ({insertId}) => {
      return {predict_id: insertId};
    })
    .catch(error => {
      if (error.code == 'ER_DUP_ENTRY') { // ER_DUP_ENTRY
        return this.selectSubPredictionLog({
          account_id, predict_material, result
        })
        .then( id => {
          return {predict_id: id};
        });
      }
      throw error;
    })
  }

  /*
   * Create Prediction Logs
   */
  insertToPredictionLog({
    account_id, 
    session_id, 
    request_id, 
    predict_material,
    succint_result,
    type, 
    algorithm, 
    dataset, 
    result}) {
    return this.insertToSubPredictionLog({
      account_id, predict_material, succint_result, type, algorithm, dataset, result, 
    })
    .then( ({predict_id})=> {
      return db.connection('aiix')
      .insert('INSERT INTO prediction_logs SET ?',
        {
          session_id, request_id, account_id, predict_id
        })
      .then( result => {
        return predict_id;
      });
    });
  }

  insertLog({req, predict_material, succint_result, data}){
    return Promise.resolve('start')
    .then(v => {
      // Prepare insertion data
      // var urls = R.split('/', req.originalUrl);
      return this.insertToPredictionLog({
        // account_id: req.session.user.account_id,
        account_id: 0,
        session_id: req.session.id,
        request_id: req.id,
        predict_material: predict_material,
        succint_result: succint_result,
        type: data.type,
        algorithm: data.algorithm,
        dataset: data.dataset,
        result: JSON.stringify(data),
      });
    })
    .then( predict_id => {
      return {predict_id: predict_id, data: data};
    })
    .catch( error => {
      log.error(error);
      throw error;
    });
  } 

  selectLogByHashCode(hash_code){
    return db.connection('aiix')
    .select('SELECT * FROM sub_prediction_logs WHERE id = ?', [hash_code])
    .then( result => {
      log.debug(result);
      return result;
    }).catch( error => {
      log.error(error);
      throw error;
    });
  }

  selectPredictResultByHashCode(hash_code){
    return Promise.resolve(hash_code)
      .then( R.bind(hash_code => {
        return this.selectLogByHashCode(hash_code);
      }, this))
      .then( log => {
        return JSON.parse(log[0].result);
      })
      .catch( error => {
        log.error(error);
        return {};
      });
  }

  selectPredictResultBySessionAndHashCode(session_id, hash_code){
    return db.connection('aiix')
    .select('SELECT * FROM prediction_logs a, sub_prediction_logs b ' +
            'WHERE a.session_id=? and a.predict_id=? and a.predict_id=b.id', 
        [session_id, hash_code])
    .then( result => {
      log.debug(result);
      return JSON.parse(result[0].result);
    }).catch( error => {
      throw new Error('SESSION_PREDICT_ID_NOT_FOUND')
    });
  }

  selectPredictResultBySessionAndPredictMaterial(session_id, material){
    return db.connection('aiix')
    .select('SELECT * FROM prediction_logs a, sub_prediction_logs b ' +
            'WHERE a.session_id=? and b.predict_material LIKE ? and a.predict_id=b.id', 
        [session_id, material])
    .then( result => {
      log.debug(result);
      return JSON.parse(result[0].result);
    }).catch( error => {
      throw new Error('SESSION_PREDICT_ID_NOT_FOUND')
    });
  }

  /*
   * Count the logs by account id
   */
  countLogsByAccountId(account_id){
    return db.connection('aiix')
    .select('SELECT count(1) as count FROM sub_prediction_logs WHERE account_id = ?', 
            [account_id])
    .then( response => {
      log.info(JSON.stringify(response[0]));
      return response[0].count;
    })
    .catch( error => {
      log.error(error);
      throw error;
    });
  }

  /*
   * Find Paging Logs 
   */
  selectPagingLogsByAccountID(page, count, account_id){
    return db.connection('aiix')
    .select(
      'SELECT id, result, type FROM sub_prediction_logs WHERE account_id = ? ORDER BY create_dt DESC LIMIT ?, ?',
      [account_id, (page - 1) * count, parseInt(count)])
    .then(results => {
      return results;
    })
    .catch( error => {
      log.error(error);
      return {};
    });
  }

  /*
   * Find Useer
   */
  selectUserByFBToken(fb_token){
    return db.connection('aiix')
    .select(
      'SELECT * FROM accounts WHERE fb_token = ?',
      [fb_token])
    .then( result => {
      log.debug(result);
      return result[0];
    })
    .catch( error => {
      log.error(error);
      throw error;
    });
  }
}

module.exports = new Dao();
