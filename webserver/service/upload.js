var multer = require('multer');
var memory_storage = multer.memoryStorage()
var rp = require('request-promise');
var request = require('request');
var Readable = require('stream').Readable;
var fs = require('fs');
var path = require('path');
var crypto = require('crypto');
var util = require('util');
var R = require('ramda');

class Multers {
  constructor(){
    this.disk = this.createDiskMulter();
    this.memory = this.createMemoryMulter();
    if (!fs.existsSync(FILE_PATH)){
      fs.mkdirSync(FILE_PATH);
    }
  }

  createMemoryMulter(){
    return multer({storage: memory_storage});
  }

  createDiskMulter(){
    return multer({
      storage: this.createDiskStorage(),
      fileFilter: function(req, file, cb){
        var ext = path.extname(file.originalname);
        ext = ext.toLowerCase();
        if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg'){
          return cb(new Error('Only images are allowed'))
        }
        cb(null, true);
      },
      limits: {
        fileSize: 1024 * 1024 * 2
      }
    });
  }

  createDiskStorage(){
    return multer.diskStorage({
      destination: function(req, file, cb) {
        cb(null, FILE_PATH);
      },
      filename: function(req, file, cb) {
        cb(null, req.id + path.extname(file.originalname));
      }
    });
  }
  checksum(str, algorithm, encoding) {
    return crypto
      .createHash(algorithm || 'md5')
      .update(str, 'utf8')
      .digest(encoding || 'hex')
  }
  changeFilename(file){
    return util.promisify(fs.readFile)(file.path)
    .then( R.bind(data => {
      var new_name = this.checksum(data) + path.extname(file.originalname);
      return new_name;
    }, this))
    .catch(err => {
      return file.filename;
    })
  }

  post_image (uri, file){
    return Promise.resolve('start')
    .then( v => {
      return this.changeFilename(file);
    })
    .then( new_file_name => {
      return {
        method: 'POST',
        uri: uri,
        timeout: 5000,
        formData: {
          image: {
            value: fs.createReadStream(file.path),
            options: {
              filename: new_file_name,
              contentType: file.mimetype
            }
          }
        }
      };
    })
    .then( options => {
      return rp(options)
    })
    .then( info => {
      return this.deleteFile(file.path, JSON.parse(info));
    })
  }
  deleteFile(path, data){
    return util.promisify(fs.unlink)(path)
    .then( v => { 
      return data;
    });
  }
  fetch_image (uri){
    var options = {
      method: 'POST',
      uri: uri
    };
    return rp(options);
  }
}

module.exports = new Multers();
